package presentacion.web;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(name = "ProductoServer", urlPatterns = {"/ProductoServer"})
public class SintomaServer extends HttpServlet {


    Gson CONVERTIR = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       resp.setContentType("text/html;charset=UTF-8");
        TreeMap mensaje = new TreeMap();

        try {
            mensaje.put("listado", sintomaDAO.consultar());
            mensaje.put("mensajeOK", "Cosulta Exitosa");
        } catch (Exception e) {
            e.printStackTrace();
            mensaje.put("mensajeERROR", "Error! : " + e.getMessage());
        }
        resp.getWriter().print(CONVERTIR.toJson(mensaje));
       
    }

}
