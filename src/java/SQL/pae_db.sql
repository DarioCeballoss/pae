-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 01, 2018 at 10:02 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pae_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sintomas`
--

CREATE TABLE `sintomas` (
  `sintoma_id` int(11) NOT NULL,
  `sintoma` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sintomas`
--

INSERT INTO `sintomas` (`sintoma_id`, `sintoma`) VALUES
(1, 'Absceso á ñ Á Ñ'),
(2, 'Acidez de estómago'),
(3, 'Aftas bucales'),
(4, 'Alopecia areata'),
(5, 'Anhidrosis'),
(6, 'Anisocoria'),
(7, 'Ascitis'),
(8, 'Asma por ejercicio'),
(9, 'Aumento del apetito'),
(10, 'Cansancio'),
(11, 'Chalación'),
(12, 'Cistitis'),
(13, 'Cloasma'),
(14, 'Daltonismo'),
(15, 'Dedo en martillo'),
(16, 'Dermatitis de las manos'),
(17, 'Dislocación'),
(18, 'Dismenorrea'),
(19, 'Dismenorrea primaria y secundaria'),
(20, 'Disminución del apetito'),
(21, 'Dolor abdominal'),
(22, 'Dolor de cadera'),
(23, 'Dolor de garganta'),
(24, 'Dolor de las piernas'),
(25, 'Dolor de mamas'),
(26, 'Dolor de oído'),
(27, 'Dolor de ojos'),
(28, 'Dolor de pies'),
(29, 'Encopresis'),
(30, 'Epilepsia'),
(31, 'Estrés'),
(32, 'Gases intestinales'),
(33, 'Gastritis'),
(34, 'Ginecomastia'),
(35, 'Halitosis'),
(36, 'Hipocondría'),
(37, 'Ictericia'),
(38, 'Inflamación del párpado'),
(39, 'Labios agrietados'),
(40, 'Mareo'),
(41, 'Mareo a causa del movimiento'),
(42, 'Morderse las uñas'),
(43, 'Neumotórax'),
(44, 'Neuralgia'),
(45, 'Neuralgia del trigémino'),
(46, 'Nistagmo'),
(47, 'Ojos rojos'),
(48, 'Ojos secos'),
(49, 'Orzuelo'),
(50, 'Palpitaciones'),
(51, 'Pérdida de olfato'),
(52, 'Pérdida de pelo'),
(53, 'Pérdida de peso'),
(54, 'Picaduras de insectos'),
(55, 'Pie cavo'),
(56, 'Pie plano'),
(57, 'Presbicia'),
(58, 'Prevención de la acidez gástrica'),
(59, 'Prostatitis'),
(60, 'Queloide'),
(61, 'Rosácea'),
(62, 'Rotura de tímpano'),
(63, 'Sangre en el semen'),
(64, 'Sangre en las heces'),
(65, 'Sarro en los dientes'),
(66, 'Seborrea'),
(67, 'Sed excesiva'),
(68, 'Sialorrea'),
(69, 'Síndrome de Gilbert'),
(70, 'Tapón de oído'),
(71, 'Temblor hereditario esencial'),
(72, 'Temblores'),
(73, 'Temblores en las manos'),
(74, 'Tenesmo'),
(75, 'Tic facial'),
(76, 'Tortícolis'),
(77, 'Tos crónica'),
(78, 'Tratamiento de la epilepsia'),
(79, 'Tratamiento de las picaduras de insectos'),
(80, 'Tratamiento del estrés'),
(81, 'Uña encarnada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sintomas`
--
ALTER TABLE `sintomas`
  ADD PRIMARY KEY (`sintoma_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sintomas`
--
ALTER TABLE `sintomas`
  MODIFY `sintoma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
